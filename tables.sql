--<ScriptOptions statementTerminator=";"/>

CREATE TABLE "SCHEMANAME"."HIGHSCORE" (
		"highscore_id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE -2147483648 MAXVALUE 2147483647 NO CYCLE CACHE 20 NO ORDER ), 
		"code" VARCHAR(12 OCTETS) NOT NULL, 
		"username" VARCHAR(50 OCTETS) NOT NULL, 
		"score" INTEGER NOT NULL
	)
	ORGANIZE BY ROW
	DATA CAPTURE NONE 
	COMPRESS NO;

CREATE TABLE "SCHEMANAME"."LEADERBOARD" (
		"leaderboard_id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE -2147483648 MAXVALUE 2147483647 NO CYCLE CACHE 20 NO ORDER ), 
		"username" VARCHAR(50 OCTETS) NOT NULL, 
		"score" INTEGER NOT NULL
	)
	ORGANIZE BY ROW
	DATA CAPTURE NONE 
	COMPRESS NO;

CREATE TABLE "SCHEMANAME"."TAGGER" (
		"tagger_id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY ( START WITH 1 INCREMENT BY 1 MINVALUE -2147483648 MAXVALUE 2147483647 NO CYCLE CACHE 20 NO ORDER ), 
		"code" VARCHAR(12 OCTETS) NOT NULL, 
		"tag" VARCHAR(50 OCTETS) NOT NULL, 
		"username" VARCHAR(50 OCTETS) NOT NULL, 
		"video_time" DOUBLE NOT NULL
	)
	ORGANIZE BY ROW
	DATA CAPTURE NONE 
	COMPRESS NO;

CREATE TABLE "SCHEMANAME"."USER" (
		"username" VARCHAR(50 OCTETS) NOT NULL, 
		"password" VARCHAR(50 OCTETS) NOT NULL, 
		"admin" INTEGER NOT NULL DEFAULT 0
	)
	ORGANIZE BY ROW
	DATA CAPTURE NONE 
	COMPRESS NO;

CREATE TABLE "SCHEMANAME"."VIDEO" (
		"code" VARCHAR(12 OCTETS) NOT NULL, 
		"url" VARCHAR(60 OCTETS) NOT NULL, 
		"thumbnailUrl" VARCHAR(60 OCTETS), 
		"title" VARCHAR(200 OCTETS) NOT NULL, 
		"desc" VARCHAR(5000 OCTETS) NOT NULL, 
		"keywords" VARCHAR(1000 OCTETS) NOT NULL, 
		"maxscoretag" VARCHAR(50 OCTETS)
	)
	ORGANIZE BY ROW
	DATA CAPTURE NONE 
	COMPRESS NO;

CREATE INDEX "SCHEMANAME"."HIGHSCORE_IDX"
	ON "SCHEMANAME"."HIGHSCORE"
	("code"		ASC)
	MINPCTUSED 0
	ALLOW REVERSE SCANS
	PAGE SPLIT SYMMETRIC
	COLLECT SAMPLED DETAILED STATISTICS
	COMPRESS NO;

CREATE INDEX "SCHEMANAME"."HIGHSCORE_IDX1"
	ON "SCHEMANAME"."HIGHSCORE"
	("score"		DESC)
	MINPCTUSED 0
	ALLOW REVERSE SCANS
	PAGE SPLIT SYMMETRIC
	COLLECT SAMPLED DETAILED STATISTICS
	COMPRESS NO;

CREATE INDEX "SCHEMANAME"."LEADERBOARD_IDX"
	ON "SCHEMANAME"."LEADERBOARD"
	("score"		DESC)
	MINPCTUSED 0
	ALLOW REVERSE SCANS
	PAGE SPLIT SYMMETRIC
	COLLECT SAMPLED DETAILED STATISTICS
	COMPRESS NO;

CREATE INDEX "SCHEMANAME"."TAGGER_IDX"
	ON "SCHEMANAME"."TAGGER"
	("code"		ASC)
	MINPCTUSED 0
	ALLOW REVERSE SCANS
	PAGE SPLIT SYMMETRIC
	COLLECT SAMPLED DETAILED STATISTICS
	COMPRESS NO;

CREATE INDEX "SCHEMANAME"."TAGGER_IDX1"
	ON "SCHEMANAME"."TAGGER"
	("tag"		ASC)
	MINPCTUSED 0
	ALLOW REVERSE SCANS
	PAGE SPLIT SYMMETRIC
	COLLECT SAMPLED DETAILED STATISTICS
	COMPRESS NO;

CREATE INDEX "SCHEMANAME"."TAGGER_IDX2"
	ON "SCHEMANAME"."TAGGER"
	("username"		ASC)
	MINPCTUSED 0
	ALLOW REVERSE SCANS
	PAGE SPLIT SYMMETRIC
	COLLECT SAMPLED DETAILED STATISTICS
	COMPRESS NO;

ALTER TABLE "SCHEMANAME"."HIGHSCORE" ADD CONSTRAINT "SQL150626135435130" PRIMARY KEY
	("highscore_id");

ALTER TABLE "SCHEMANAME"."LEADERBOARD" ADD CONSTRAINT "SQL150626132635160" PRIMARY KEY
	("leaderboard_id");

ALTER TABLE "SCHEMANAME"."TAGGER" ADD CONSTRAINT "SQL150628104019850" PRIMARY KEY
	("tagger_id");

ALTER TABLE "SCHEMANAME"."USER" ADD CONSTRAINT "SQL150626132048940" PRIMARY KEY
	("username");

ALTER TABLE "SCHEMANAME"."VIDEO" ADD CONSTRAINT "SQL150626133623850" PRIMARY KEY
	("code");

ALTER TABLE "SCHEMANAME"."HIGHSCORE" ADD CONSTRAINT "HIGHSCORE_VIDEO_FK" FOREIGN KEY
	("code")
	REFERENCES "SCHEMANAME"."VIDEO"
	("code")
	ON DELETE CASCADE
	ON UPDATE RESTRICT;

ALTER TABLE "SCHEMANAME"."LEADERBOARD" ADD CONSTRAINT "LEADERBOARD_USER_FK" FOREIGN KEY
	("username")
	REFERENCES "SCHEMANAME"."USER"
	("username")
	ON DELETE CASCADE
	ON UPDATE RESTRICT;

ALTER TABLE "SCHEMANAME"."TAGGER" ADD CONSTRAINT "TAGGER_USER_FK" FOREIGN KEY
	("username")
	REFERENCES "SCHEMANAME"."USER"
	("username")
	ON DELETE CASCADE
	ON UPDATE RESTRICT;

ALTER TABLE "SCHEMANAME"."TAGGER" ADD CONSTRAINT "TAGGER_VIDEO_FK" FOREIGN KEY
	("code")
	REFERENCES "SCHEMANAME"."VIDEO"
	("code")
	ON DELETE CASCADE
	ON UPDATE RESTRICT;
